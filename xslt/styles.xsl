<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
  
  <xsl:attribute-set name="sty.default">
    <xsl:attribute name="font-family">Serif</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
    <xsl:attribute name="color">#000000</xsl:attribute>
    <xsl:attribute name="background-color">#FFFFFF</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page">
    <xsl:attribute name="page-height">210mm</xsl:attribute>
    <xsl:attribute name="page-width">297mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page.body">
    <xsl:attribute name="margin-left">20mm</xsl:attribute>
    <xsl:attribute name="margin-right">20mm</xsl:attribute>
    <xsl:attribute name="margin-top">15mm</xsl:attribute>
    <xsl:attribute name="margin-bottom">15mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page.header-height">
    <xsl:attribute name="extent">5mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page.footer-height">
    <xsl:attribute name="extent">5mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page.header">
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="border-top-style">none</xsl:attribute>
    <xsl:attribute name="border-top-width">thin</xsl:attribute>
    <xsl:attribute name="border-top-color">gray</xsl:attribute>
    <xsl:attribute name="font-family">Sans</xsl:attribute>
    <xsl:attribute name="font-size">0.8em</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="start-indent">5mm</xsl:attribute>
    <xsl:attribute name="end-indent">5mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.page.footer">
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="border-top-style">none</xsl:attribute>
    <xsl:attribute name="border-top-width">thin</xsl:attribute>
    <xsl:attribute name="border-top-color">gray</xsl:attribute>
    <xsl:attribute name="font-family">Sans</xsl:attribute>
    <xsl:attribute name="font-size">0.8em</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="start-indent">5mm</xsl:attribute>
    <xsl:attribute name="end-indent">5mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.border-top">
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="padding-top">0.2em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.title">
    <xsl:attribute name="font-family">Serif</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="keep-with-next">always</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.title.1" use-attribute-sets="sty.title sty.border-top">
    <xsl:attribute name="border-top-color">black</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="font-size">1.4em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.title.2" use-attribute-sets="sty.title sty.border-top">
    <xsl:attribute name="border-color">black</xsl:attribute>
    <xsl:attribute name="border-width">medium</xsl:attribute>
    <xsl:attribute name="font-size">1.2em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.title.3" use-attribute-sets="sty.title">
    <xsl:attribute name="border-top-style">none</xsl:attribute>
    <xsl:attribute name="border-color">black</xsl:attribute>
    <xsl:attribute name="border-width">thin</xsl:attribute>
    <xsl:attribute name="font-size">1.0em</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.personname">
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-family">Serif</xsl:attribute>
    <xsl:attribute name="font-size">1em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.graphic">
    <xsl:attribute name="border-color">black</xsl:attribute>
    <xsl:attribute name="border-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">thin</xsl:attribute>
    <xsl:attribute name="content-width">scale-down-to-fit</xsl:attribute>
    <xsl:attribute name="height">30mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.section">
    <xsl:attribute name="space-before.optimum">1.2em</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0.6em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.8em</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="sty.emphasis">
    <xsl:attribute name="font-size">1.0em</xsl:attribute>
    <xsl:attribute name="font-weight">inherit</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="font-family">inherit</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>